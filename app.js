var state
let uniqueNo = 1
let governor = 5

function * uniqueNumber () {
  while (true) {
    yield(uniqueNo++)
  }
}

function directionTo (item1, item2) {
  const x = item1.location.x - item2.location.x
  const y = item1.location.y - item2.location.y
  const h = Math.sqrt(x*x + y*y)
  return new Direction(x/h, y/h)
}

function directionAround (item1, item2) {
  const x = item1.location.x - item2.location.x
  const y = item1.location.y - item2.location.y
  const h = Math.sqrt(Math.sqrt(x*x + y*y))
  return new Direction(-x/h, -y/h)
}

function distanceBetween (item1, item2) {
  const x = item1.location.x - item2.location.x
  const y = item1.location.y - item2.location.y
  return Math.sqrt(x*x + y*y)
}

function randomInteger (max) {
  return Math.floor(Math.random() * (max + 1))
}

function randomFloat (max) {
  return randomInteger(max * 100)/100
}

function getState () {
  var state = state || new State()
  return state
}


class Point {
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  static random (x, y) {
    return new Point(randomFloat(x), randomFloat(y))
  }

  static add (p, p1) {
    return {
      x: p.x + p1.x,
      y: p.y + p1.y
    }
  }

  static divide (p, amt) {
    return {
      x: p.x/amt,
      y: p.y/amt
    }
  }
}

class Direction {
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  normalize () {
    let h = Math.sqrt(this.x * this.x + this.y * this.y)
    return new Direction(this.x/h, this.y/h)
  }

  add (dir) {
    this.x += dir.x
    this.y += dir.y
  }

  static average (dir1, dir2) {
    return new Direction((dir1.x + dir2.x) / 2, (dir1.y + dir2.y) / 2)
  }
}

class Item {
  constructor (name, type, point) {
    this.location = point
    this.name = name
    this.type = type
    this.id = uniqueNumber().next().value
  }

  direction (bot) {
    return directionTo(bot, this)
  }

  distance (bot) {
    return distanceBetween(bot, this)
  }
}




class AtCondition {
  constructor (itemName) {
    this.itemName = itemName
  }

  inEffect (bot) {
    const item = bot.findItem(this.itemName)
    return distanceBetween(item, bot) <= 1
  }
}

class AfterCondition {
  constructor (eventName) {
    this.eventName = eventName
  }

  inEffect (bot) {
    return bot.findEvent(this.eventName)
  }
}

class Logger {
  constructor (name) {
    this.name = name
  }

  inEffect (bot) {
    return !this.condition || this.condition.inEffect(bot)
  }

  check (bot) {
    if (this.inEffect(bot)) {
      bot.addEvent(this.name)
    }
  }

  at (itemName) {
    this.condition = new AtCondition(itemName)
  }
}


class Event {
  constructor (name) {
    this.name = name
    this.date = Date.now()
  }
}

class Force {
  constructor (itemName) {
    this.itemName = itemName
  }
  direction (bot) {
    return directionTo(bot.findItem(this.itemName), bot)
  }
  distance (bot) {
    return distanceBetween(bot.findItem(this.itemName), bot)
  }
  power (bot) {
    return this.direction(bot)/(bot.findItem(this.itemName).distance(bot) * bot.findItem(this.itemName).distance(bot))
  }
  effect (bot) {
    return this.direction(bot)
  }
  inEffect (bot) {
    return !this.condition || this.condition.inEffect(bot)
  }
  after (loggerText) {
    this.condition = new AfterCondition(loggerText)
  }
}

class AvoidForce extends Force {
  direction (bot) {
    return directionAround(bot.findItem(this.itemName), bot)
  }
  effect (bot) {
    let effect = this.direction(bot)
    effect.x = effect.x * 4/(bot.findItem(this.itemName).distance(bot) * bot.findItem(this.itemName).distance(bot) * bot.findItem(this.itemName).distance(bot))
    effect.y = effect.y * 4/(bot.findItem(this.itemName).distance(bot) * bot.findItem(this.itemName).distance(bot) * bot.findItem(this.itemName).distance(bot))
    return effect
  }
}

class Avoid {
  constructor (itemType) {
    this.itemType = itemType
  }
}


class Bot {
  constructor (location) {
    this.location = location
    this.momentum = new Direction(0, 0)
    this.type = 'bot'
    this.name = 'You'
    this.items = new Array()
    this.events = new Array()
    this.forces = new Array()
    this.avoids = new Array()
    this.loggers = new Array()
    this.addEvent('start')
    this.addItem(this)
  }

  seek (itemName) {
    const force = new Force(itemName)
    this.forces.push(force)
    return force
  }

  avoid (itemType) {
    const avoid = new Avoid(itemType)
    this.avoids.push(avoid)
    return avoid
  }

  avoidTypes () {
    return this.avoids.map(a => a.itemType)
  }

  avoidItems () {
    return this.items.filter(i => this.avoidTypes().includes(i.type) && i.distance(this) < 10)
  }

  log (loggerText) {
    const logger = new Logger(loggerText)
    this.loggers.push(logger)
    return logger
  }

  addEvent(eventName) {
    const newEvent = new Event(eventName)
    const lastEvent = this.events[this.events.length - 1]
    if (!lastEvent || newEvent.date - lastEvent.date > 300 ) {
      this.events.push(new Event(eventName))
    }
  }

  sortedForces () {
    return this.forces.sort((a, b) => {
      if (a.condition && !b.condition) {
        return 100000
      } else if (!a.condition && b.condition) {
        return -100000
      }
      return (this.findEvent(a.condition.eventName) ? this.findEvent(a.condition.eventName).date : 0) - (this.findEvent(b.condition.eventName) ? this.findEvent(b.condition.eventName).date : 0)
    })
  }

  currentForce () {
    return this.sortedForces().filter(f => f.inEffect(this)).pop()
  }

  pull () {
    let a = new Direction(0, 0)
    let forces = [this.currentForce()].concat(this.avoidItems().map(item => (new AvoidForce(item.name))))
    forces.forEach(force => a.add(force.effect(this)))
    return Direction.average(a.normalize(), this.momentum)
  }

  nextLocation () {
    this.momentum = this.pull()
    return Point.add(this.location, Point.divide(this.momentum, governor))
  }

  move (location) {
    if (location) {
      this.location = location
    } else {
      this.location = this.nextLocation()
    }
    this.loggers.forEach(logger => logger.check(this))
    return this.pull()
  }

  findEvent (eventName) {
    return this.events.slice().reverse().find(e => e.name === eventName)
  }

  addItem (item) {
    this.items.push(item)
  }

  removeItem (itemName) {
    const item = this.findItem(itemName)
    item.type = 'delete'
  }

  removeItems (itemType) {
    const items = this.items.filter(i => i.type === itemType)
    items.forEach(item => {
      this.removeItem(item.name)
    })
  }

  findItem (itemName) {
    return this.items.find(item => item.name === itemName)
  }

  removeOldItems () {
    const items = this.items.filter(i => i.type === 'delete')
    items.forEach(item => {
      const index = this.items.indexOf(item);
      if (index !== -1) this.items.splice(index, 1);
    })
  }

}