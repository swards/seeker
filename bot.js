
class Bot {
  constructor (location) {
    this.location = location
    this.type = 'bot'
    this.name = 'You'
    this.events = new Array()
    this.forces = new Array()
    this.loggers = new Array()
    this.events.push(new Item('start', 'system', null, Date.now()))
  }

  seek (itemName) {
    this.forces.push(new Force(itemName))
  }

  log (loggerText) {
    logger = new Logger(loggerText)
    this.loggers.push(logger)
    return logger
  }

  pull () {
    let x = 0
    let y = 0
    for (let i = 0; i < this.forces.length; i++) {
      console.log(this.forces[i].effect(this))
      x += this.forces[i].effect(this).x
      y += this.forces[i].effect(this).y
    }
    return { x, y }
  }
}
