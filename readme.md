SCR (seeker)
==============

Seeker is a simple api for directing bots or other independent motorized devices around an area filled with preferred paths, obstacles, goals and environment state.

- [Pre-Proposal](https://docs.google.com/document/d/1QcuKgaNIwCXhmIXBtwJPh4bTSq410IRg6fXioHy3ZSM/edit?usp=sharing)
- [Proposal](https://docs.google.com/document/d/10KzsymXm6rEkWY8VXhAxAonJwPT1QHK_5uW1eLy72Vw/edit?usp=sharing)
- [Design](https://docs.google.com/document/d/19S8Jf6Xq9kCpBt8vhq0LwrvAANVkJvtwubbu5_ZA82g/edit?usp=sharing)