class World {
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  render (bot) {
    let results = ''
    if (bot) {
      for (let i = 0; i < bot.items.length; i++) {
        const xP = bot.items[i].location.x * 100.0 / this.x
        const yP = bot.items[i].location.y * 100.0 / this.y
        results += `<div title="${bot.items[i].type} ${bot.items[i].name}" class="${bot.items[i].type}" id="${bot.items[i].name}" style="left:${xP}%;bottom:${yP}%"></div>`
      }
    }
    return results
  }
}