class Actor {
    constructor () {
        this.locations = new Array()
    }

    goTo (location) {
        this.locations.push(location)
    }
    whenAt (location) {
        this.locations.push(location)
    }

    whereTo () {
        return this.locations[0]
    }
}



r = new Actor();
r.goTo('home');
console.log(r.whereTo())

Point = [x, y];

state = {
    position: Point,
    status: '',
    events: [
        {
            type: EventType,
            itemType: ItemType,
            time: integer
        }
    ],
    items: [
        {
            location: Point,
            type: ItemType,
            name: ''
        }
    ]
}

Bot
  goals: []

Condition
  AtCondition
    'name' | 'type'

  NearCondition
    'name' | 'type'

Action

TowardDirection
  target: ''
AwayDirection
  target: ''
CrossDirection
  target: ''

DoGoal
  conditions:
  action:

GoGoal
  conditions:
  directions: []



toward('name') // returns a direction
towardsOldest('type') // passed an item, finds position, returns a direction (x, y from a triangle)
avoid('name') // passed an item, goes orthogonal
awayFrom('name') // passed an item, finds position, goes away from this item

at(item) // returns self, if true
near(item) // returns self, if true
recently(EventType) // returns a bot

do() // Adds a DoGoal to the bot
go() // Adds a GoGoal to the bot


bot.goTowardOldest('Checkpoint');

bot.at('Checkpoint').do('CheckIn', 'Checkpoint');
bot.at("").status('');

bot.process(state);

state.process(bot);  // this modifies state and returns the set of changes and

